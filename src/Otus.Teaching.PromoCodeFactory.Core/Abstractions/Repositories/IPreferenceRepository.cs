﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromocodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IPreferenceRepository : IRepository<Preference>
    {
        Task<Preference> GetByNameAsync(string name);
    }
}
