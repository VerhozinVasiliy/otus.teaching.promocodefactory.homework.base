﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromocodeManagement;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface IPromocodeRepository : IRepository<PromoCode>
    {
        Task<PromoCode> GetByCodeAsync(string code);
    }
}
