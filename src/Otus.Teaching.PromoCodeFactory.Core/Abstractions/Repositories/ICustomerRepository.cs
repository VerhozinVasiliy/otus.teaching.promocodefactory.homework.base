﻿using Otus.Teaching.PromoCodeFactory.Core.Domain.PromocodeManagement;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories
{
    public interface ICustomerRepository : IRepository<Customer>
    {
        Task SetPromocodeToCustomers(Guid promocodeId, IList<Guid> customersIdList);
    }
}
