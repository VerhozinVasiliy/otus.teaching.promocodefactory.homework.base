﻿using System.Collections.Generic;

namespace Otus.Teaching.PromoCodeFactory.Core.Domain.PromocodeManagement
{
    public class Preference : BaseEntity
    {
        public string Name { get; set; }
        public string Description { get; set; }

        public IList<PromoCode> Promocodes { get; set; }
        public IList<CustomerPreference> CustomerPreferences { get; set; }
    }
}
