﻿using System;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Models
{
    public class PromocodeRequest
    {
        public string Code { get; set; }
        public string Preference { get; set; }
        public string BeginDate { get; set; }
        public string EndDate { get; set; }
    }
}
