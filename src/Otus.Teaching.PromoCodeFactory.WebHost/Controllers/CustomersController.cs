﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromocodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Клиенты
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class CustomersController : ControllerBase
    {
        private readonly ICustomerRepository _customersRepository;
        private readonly IRepository<Preference> _preferenceRepository;
        private readonly IRepository<CustomerPreference> _customerPreferenceRepository;
        private readonly IRepository<PromoCode> _promocodeRepository;
        public CustomersController(
            ICustomerRepository customersRepository, 
            IRepository<Preference> preferenceRepository, 
            IRepository<CustomerPreference> customerPreferenceRepository,
            IRepository<PromoCode> promocodeRepository)
        {
            _customersRepository = customersRepository;
            _preferenceRepository = preferenceRepository;
            _customerPreferenceRepository = customerPreferenceRepository;
            _promocodeRepository = promocodeRepository;
        }

        /// <summary>
        /// Получить всех клиентов
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IList<CustomerShortResponse>> GetCustomersAsync()
        {
            var customers = await _customersRepository.GetAllAsync();

            var customersModelList = customers.Select(x =>
                new CustomerShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FirstName = x.FirstName,
                    LastName = x.LastName
                }).ToList();

            return customersModelList;
        }

        /// <summary>
        /// Получить клиента по Id
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public async Task<ActionResult<CustomerResponse>> GetCustomerAsync(Guid id)
        {
            var customer = await _customersRepository.GetByIdAsync(id);
            if (customer is null)
            {
                return NotFound();
            }
            var preferencesList = new List<PreferenceResponse>();
            if (customer.CustomerPreferences.Any())
            {
                var preferencesFromBase = await _preferenceRepository.GetByListIdAsync(customer.CustomerPreferences.Select(x => x.PreferenceId).ToList());
                if (!preferencesFromBase.Any())
                {
                    return NotFound();
                }
                preferencesList = preferencesFromBase.Select(x => new PreferenceResponse() { Id = x.Id, Name = x.Name }).ToList();
            }

            var promocode = customer.Promocode;
            return new CustomerResponse()
            {
                Id = customer.Id,
                Email = customer.Email,
                FirstName = customer.FirstName,
                LastName = customer.LastName,
                Preferences = preferencesList,
                PromoCodes = promocode == null ? null :
                    new PromoCodeShortResponse()
                    {
                        Id = promocode.Id,
                        Code = promocode.Code,
                        BeginDate = promocode.BeginDate.ToShortDateString(),
                        EndDate = promocode.EndDate.ToShortDateString(),
                        Preference = promocode.Preference?.Name
                    }
            };
        }

        /// <summary>
        /// Добавить клиента с предпочтениями
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> CreateCustomerAsync(CreateOrEditCustomerRequest request)
        {
            var customerGuid = Guid.NewGuid();
            var customer = new Customer()
            {
                Id = customerGuid,
                FirstName = request.FirstName,
                LastName = request.LastName,
                Email = request.Email
            };

            await _customersRepository.AddNewAsync(customer);

            await AddPreferenceToCustomer(request, customerGuid);

            return Ok();
        }

        /// <summary>
        /// Изменить клиента с предпочтениями
        /// </summary>
        /// <param name="id"></param>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPut("{id}")]
        public async Task<IActionResult> EditCustomersAsync(Guid id, CreateOrEditCustomerRequest request)
        {
            var customer = await _customersRepository.GetByIdAsync(id);
            if (customer is null)
            {
                return NotFound();
            }

            customer.FirstName = request.FirstName;
            customer.LastName = request.LastName;
            customer.Email = request.Email;

            if (customer.CustomerPreferences.Any())
            {
                await _customerPreferenceRepository.DeleteByListIdsAsync(customer.CustomerPreferences.Select(x => x.Id).ToList());
            }

            await AddPreferenceToCustomer(request, customer.Id);

            return Ok();
        }

        /// <summary>
        /// Удаление клиента
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /// <exception cref="NotImplementedException"></exception>
        [HttpDelete]
        public async Task<IActionResult> DeleteCustomer(Guid id)
        {
            var customer = await _customersRepository.GetByIdAsync(id);
            if (customer is null)
            {
                return NotFound();
            }

            if (customer.CustomerPreferences.Any())
            {
                await _customerPreferenceRepository.DeleteByListIdsAsync(customer.CustomerPreferences.Select(x => x.Id).ToList());
            }

            var promocode = customer.Promocode;
            if (promocode != null)
            {
                if (promocode.Customers.Count == 1)
                {
                    await _promocodeRepository.DeleteAsync(promocode.Id);
                }
            }

            await _customersRepository.DeleteAsync(customer.Id);

            return Ok();
        }

        /// <summary>
        /// Добавление предпочтений для клиента
        /// </summary>
        /// <param name="request"></param>
        /// <param name="customerGuid"></param>
        /// <returns></returns>
        private async Task AddPreferenceToCustomer(CreateOrEditCustomerRequest request, Guid customerGuid)
        {
            if (request.PreferenceIds == null || !request.PreferenceIds.Any())
            {
                return;
            }
            var preferences = await _preferenceRepository.GetByListIdAsync(request.PreferenceIds);
            if (!preferences.Any())
            {
                return;
            }

            foreach (var preference in preferences)
            {
                var customerPreference = new CustomerPreference()
                {
                    CustomerId = customerGuid,
                    PreferenceId = preference.Id,
                };
                await _customerPreferenceRepository.AddNewAsync(customerPreference);
            }
        }
    }
}
