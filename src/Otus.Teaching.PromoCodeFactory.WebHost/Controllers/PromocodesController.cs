﻿using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromocodeManagement;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Промокоды
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class PromocodesController : ControllerBase
    {
        private readonly IPromocodeRepository _promocodeRepository;
        private readonly ICustomerRepository _customersRepository;
        private readonly IPreferenceRepository _preferenceRepository;

        public PromocodesController(IPromocodeRepository promocodeRepository, ICustomerRepository customersRepository, IPreferenceRepository preferenceRepository)
        {
            _promocodeRepository = promocodeRepository;
            _customersRepository = customersRepository;
            _preferenceRepository = preferenceRepository;
        }

        /// <summary>
        /// Сохранить и выдать промокод
        /// </summary>
        /// <param name="request"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<IActionResult> AddPromocodeAsync(PromocodeRequest request)
        {
            if (string.IsNullOrEmpty(request.Code) || string.IsNullOrEmpty(request.Preference))
            {
                return BadRequest();
            }

            if (!DateTime.TryParse(request.BeginDate, out DateTime parsedBeginDate) || !DateTime.TryParse(request.EndDate, out DateTime parsedEndDate))
            {
                return BadRequest();
            }

            var existPromocode = await _promocodeRepository.GetByCodeAsync(request.Code);
            if (existPromocode != null)
            {
                return Ok(existPromocode);
            }

            var preference  = await _preferenceRepository.GetByNameAsync(request.Preference);
            if (preference == null)
            {
                return NotFound();
            }

            var promocodeId = Guid.NewGuid();
            var newPromocode = new PromoCode()
            {
                Id = promocodeId,
                Code = request.Code,
                PreferenceID = preference.Id,
                BeginDate = parsedBeginDate,
                EndDate = parsedEndDate
            };

            await _promocodeRepository.AddNewAsync(newPromocode);

            var customerPreferences = preference.CustomerPreferences;
            if (customerPreferences == null || !customerPreferences.Any())
            {
                return Ok();
            }

            await _customersRepository.SetPromocodeToCustomers(promocodeId, preference.CustomerPreferences.Select(x => x.CustomerId).ToList());

            return Ok();
        }

        /// <summary>
        /// Получить все доступные промокоды
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<PromoCodeShortResponse>> GetPromocodesAsync()
        {
            var promoCodes = await _promocodeRepository.GetAllAsync();
            if (promoCodes == null || !promoCodes.Any())
            {
                return new List<PromoCodeShortResponse>();
            }

            return promoCodes.Select(x =>
                new PromoCodeShortResponse()
                {
                    Id = x.Id,
                    Code = x.Code,
                    Preference = x.Preference.Name,
                    BeginDate = x.BeginDate.ToShortDateString(),
                    EndDate = x.EndDate.ToShortDateString()
                }).ToList();
        }
    }
}
