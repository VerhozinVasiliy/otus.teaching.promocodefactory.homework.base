﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.WebHost.Models;

namespace Otus.Teaching.PromoCodeFactory.WebHost.Controllers
{
    /// <summary>
    /// Сотрудники
    /// </summary>
    [ApiController]
    [Route("api/v1/[controller]")]
    public class EmployeesController
        : ControllerBase
    {
        private readonly IEmployeeRepository _employeeRepository;
        private readonly IRepository<Role> _rolesRepository;

        public EmployeesController(IEmployeeRepository employeeRepository, IRepository<Role> rolesRepository)
        {
            _employeeRepository = employeeRepository;
            _rolesRepository = rolesRepository;
        }

        /// <summary>
        /// Получить данные всех сотрудников
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<List<EmployeeShortResponse>> GetEmployeesAsync()
        {
            var employees = await _employeeRepository.GetAllAsync();

            var employeesModelList = employees.Select(x =>
                new EmployeeShortResponse()
                {
                    Id = x.Id,
                    Email = x.Email,
                    FullName = x.FullName,
                }).ToList();

            return employeesModelList;
        }

        /// <summary>
        /// Получить данные сотрудника по Id
        /// </summary>
        /// <returns></returns>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<EmployeeResponse>> GetEmployeeByIdAsync(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var employeeModel = new EmployeeResponse()
            {
                Id = employee.Id,
                Email = employee.Email,
                Role = new RoleItemResponse()
                {
                    Id = employee.RoleId,
                    Name = employee.Role.Name,
                    Description = employee.Role.Description
                },
                FullName = employee.FullName,
                AppliedPromocodesCount = employee.AppliedPromocodesCount
            };

            return employeeModel;
        }

        /// <summary>
        /// Добавить сотрудника
        /// </summary>
        /// <param name="employeeModel"></param>
        /// <returns></returns>
        [HttpPost]
        public async Task<ActionResult> CreateEmployee(EmployeeCreateUpdateModel employeeModel)
        {
            var roles = await _rolesRepository.GetAllAsync();
            var myRole = roles.FirstOrDefault(x => x.Name == employeeModel.RoleName);
            if (myRole == null)
                return NotFound();

            var newEmploeey = new Employee
            {
                Id = Guid.NewGuid(),
                FirstName = employeeModel.FirstName,
                LastName = employeeModel.LastName,
                Email = employeeModel.Email,
                Role = myRole,
                AppliedPromocodesCount = 0
            };

            await _employeeRepository.AddNewAsync(newEmploeey);

            return Ok();
        }

        /// <summary>
        /// Удалить сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpDelete("id:guid")]
        public async Task<ActionResult> DeleteEmployee(Guid id)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            await _employeeRepository.DeleteAsync(id);

            return Ok();
        }

        /// <summary>
        /// Редактировать сотрудника
        /// </summary>
        /// <param name="id"></param>
        /// <param name="employeeModel"></param>
        /// <returns></returns>
        [HttpPut("id:guid")]
        public async Task<ActionResult> UpdateEmployee(Guid id, EmployeeCreateUpdateModel employeeModel)
        {
            var employee = await _employeeRepository.GetByIdAsync(id);

            if (employee == null)
                return NotFound();

            var roles = await _rolesRepository.GetAllAsync();
            var myRole = roles.FirstOrDefault(x => x.Name == employeeModel.RoleName);
            if (myRole == null)
                return NotFound();

            employee.FirstName = employeeModel.FirstName;
            employee.LastName = employeeModel.LastName;
            employee.Email = employeeModel.Email;
            employee.Role = myRole;
            employee.AppliedPromocodesCount = employeeModel.AppliedPromocodesCount;

            await _employeeRepository.UpdateAsync(employee);

            return Ok();
        }
    }
}