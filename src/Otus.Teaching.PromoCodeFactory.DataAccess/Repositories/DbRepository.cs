﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class DbRepository<T>
        : IRepository<T>
        where T : BaseEntity
    {
        protected readonly DatabaseContext _dataContext;

        public DbRepository(DatabaseContext dataContext)
        {
            _dataContext = dataContext;
        }

        public async Task AddNewAsync(T data)
        {
            await _dataContext.Set<T>().AddAsync(data);

            await _dataContext.SaveChangesAsync();
        }

        public async Task DeleteAsync(Guid id)
        {
            var el = await _dataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
            if (el != null)
            {
                _dataContext.Remove(el);
                await _dataContext.SaveChangesAsync();
            }
        }

        public async Task DeleteByListIdsAsync(IList<Guid> idList)
        {
            var elList = await _dataContext.Set<T>().Where(x => idList.Contains(x.Id)).ToListAsync();
            if (elList.Any())
            {
                _dataContext.RemoveRange(elList);
                await _dataContext.SaveChangesAsync();
            }
        }

        public virtual async Task<IEnumerable<T>> GetAllAsync()
        {
            var entities = await _dataContext.Set<T>().ToListAsync();

            return entities;
        }

        public virtual async Task<T> GetByIdAsync(Guid id)
        {
            return await _dataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id);
        }

        public async Task UpdateAsync(T data)
        {
            await _dataContext.SaveChangesAsync();
        }

        public async Task<IList<T>> GetByListIdAsync(IList<Guid> listId)
        {
            var foundedList = _dataContext.Set<T>().AsQueryable();
            foundedList = foundedList.Where(x => listId.Contains(x.Id));
            return await foundedList.ToListAsync();
        }
    }
}
