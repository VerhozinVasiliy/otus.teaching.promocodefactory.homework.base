﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class EmploeeyRepository : DbRepository<Employee>, IEmployeeRepository
    {
        public EmploeeyRepository(DatabaseContext context) : base(context)
        {
        }

        public override async Task<Employee> GetByIdAsync(Guid id)
        {
            var query = _dataContext.Set<Employee>().AsQueryable();

            query = query
                .Include(c => c.Role)
                .Where(c => c.Id == id);
            
            return await query.SingleOrDefaultAsync();
        }
    }
}
