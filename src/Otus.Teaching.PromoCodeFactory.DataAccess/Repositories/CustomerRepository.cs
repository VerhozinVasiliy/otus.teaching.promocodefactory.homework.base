﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromocodeManagement;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class CustomerRepository : DbRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository(DatabaseContext dataContext) : base(dataContext)
        {

        }

        public override async Task<Customer> GetByIdAsync(Guid id)
        {
            var query = _dataContext.Set<Customer>().AsQueryable();

            query = query
                .Include(c => c.CustomerPreferences)
                .Include(c => c.Promocode)
                .Where(c => c.Id == id);

            return await query.SingleOrDefaultAsync();
        }

        public async Task SetPromocodeToCustomers(Guid promocodeId, IList<Guid> customersIdList)
        {
            var customers = await GetByListIdAsync(customersIdList);
            if (customers == null || !customers.Any())
            {
                return;
            }

            foreach (var customer in customers)
            {
                customer.PromocodeId = promocodeId;
            }

            await _dataContext.SaveChangesAsync();
        }
    }
}
