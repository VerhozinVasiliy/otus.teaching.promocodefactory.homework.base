﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromocodeManagement;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class PromocodeRepository : DbRepository<PromoCode>, IPromocodeRepository
    {
        public PromocodeRepository(DatabaseContext dataContext) : base(dataContext)
        {
        }

        public async Task<PromoCode> GetByCodeAsync(string code)
        {
            var query = _dataContext.Set<PromoCode>().AsQueryable();

            query = query
                .Where(c => c.Code == code);

            return await query.SingleOrDefaultAsync();
        }

        public override async Task<IEnumerable<PromoCode>> GetAllAsync()
        {
            var query = _dataContext.Set<PromoCode>().AsQueryable();

            query = query
                .Include(c => c.Preference);

            return await query.ToListAsync();
        }
    }
}
