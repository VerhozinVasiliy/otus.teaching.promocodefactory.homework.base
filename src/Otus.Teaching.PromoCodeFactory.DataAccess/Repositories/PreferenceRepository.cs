﻿using Microsoft.EntityFrameworkCore;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromocodeManagement;
using System.Linq;
using System.Threading.Tasks;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class PreferenceRepository : DbRepository<Preference>, IPreferenceRepository
    {
        public PreferenceRepository(DatabaseContext dataContext) : base(dataContext)
        {
        }

        public async Task<Preference> GetByNameAsync(string name)
        {
            var query = _dataContext.Set<Preference>().AsQueryable();

            query = query
                .Include(x => x.CustomerPreferences)
                .Where(c => c.Name == name);

            return await query.SingleOrDefaultAsync();
        }
    }
}
