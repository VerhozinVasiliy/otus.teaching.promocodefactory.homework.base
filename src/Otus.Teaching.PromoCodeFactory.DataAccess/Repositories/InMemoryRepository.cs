﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Otus.Teaching.PromoCodeFactory.Core.Abstractions.Repositories;
using Otus.Teaching.PromoCodeFactory.Core.Domain;

namespace Otus.Teaching.PromoCodeFactory.DataAccess.Repositories
{
    public class InMemoryRepository<T>
        : IRepository<T>
        where T: BaseEntity
    {
        protected IEnumerable<T> Data { get; set; }

        public InMemoryRepository(IEnumerable<T> data)
        {
            Data = data;
        }
        
        public Task<IEnumerable<T>> GetAllAsync()
        {
            return Task.FromResult(Data);
        }

        public Task<T> GetByIdAsync(Guid id)
        {
            return Task.FromResult(Data.FirstOrDefault(x => x.Id == id));
        }

        public Task AddNewAsync(T data)
        {
            List<T> newData = Data.ToList();
            newData.Add(data);
            Data = newData;
            return Task.CompletedTask;
        }

        public Task DeleteAsync(Guid id)
        {
            List<T> newData = Data.ToList();
            newData.Remove(newData.FirstOrDefault(x => x.Id == id));
            Data = newData;
            return Task.CompletedTask;
        }

        public Task UpdateAsync(T item)
        {
            var changedItem = Data.FirstOrDefault(x => x.Id == item.Id);
            changedItem = item;
            return Task.CompletedTask;
        }

        public Task<IList<T>> GetByListIdAsync(IList<Guid> listId)
        {
            IList<T> returnedlist = Data.Where(x => listId.Contains(x.Id)).ToList();
            return Task.FromResult(returnedlist);
        }

        public Task DeleteByListIdsAsync(IList<Guid> idList)
        {
            List<T> newData = Data.Where(x => !idList.Contains(x.Id)).ToList();
            Data = newData;
            return Task.CompletedTask;
        }
    }
}