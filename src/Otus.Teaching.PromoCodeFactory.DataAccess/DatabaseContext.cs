﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Otus.Teaching.PromoCodeFactory.Core.Domain.Administration;
using Otus.Teaching.PromoCodeFactory.Core.Domain.PromocodeManagement;
using System;

namespace Otus.Teaching.PromoCodeFactory.DataAccess
{
    public class DatabaseContext : DbContext
    {
        public DatabaseContext()
        {
        }

        public DatabaseContext(DbContextOptions<DatabaseContext> options) : base(options)
        {
        }

        public DbSet<Employee> Employees { get; set; }

        public DbSet<Role> Roles { get; set; }

        public DbSet<Customer> Customers { get; set; }

        public DbSet<PromoCode> PromoCodes { get; set; }

        public DbSet<Preference> Preferences { get; set; }

        public DbSet<CustomerPreference> CustomerPreferences { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.LogTo(Console.WriteLine, LogLevel.Information);
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Employee>()
                .HasOne(bc => bc.Role)
                .WithMany(g => g.Emloyees)
                .HasForeignKey(s => s.RoleId);

            modelBuilder.Entity<PromoCode>()
                .HasOne(bc => bc.Preference)
                .WithMany(g => g.Promocodes)
                .HasForeignKey(s => s.PreferenceID);

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(bc => bc.Customer)
                .WithMany(w => w.CustomerPreferences)
                .HasForeignKey(s => s.CustomerId);

            modelBuilder.Entity<CustomerPreference>()
                .HasOne(bc => bc.Preference)
                .WithMany(w => w.CustomerPreferences)
                .HasForeignKey(s => s.PreferenceId);

            modelBuilder.Entity<CustomerPreference>().HasKey(sc => new { sc.CustomerId, sc.PreferenceId });

            modelBuilder.Entity<PromoCode>()
                .HasOne(bc => bc.Preference)
                .WithMany(g => g.Promocodes)
                .HasForeignKey(s => s.PreferenceID);

            modelBuilder.Entity<Customer>()
                .HasOne(e => e.Promocode)
                .WithMany(e => e.Customers)
                .HasForeignKey(k => k.PromocodeId);

            modelBuilder.Entity<Employee>().Property(p => p.FirstName).HasMaxLength(20);
            modelBuilder.Entity<Employee>().Property(p => p.LastName).HasMaxLength(30);
            modelBuilder.Entity<Employee>().Property(p => p.Email).HasMaxLength(100);

            modelBuilder.Entity<Role>().Property(p => p.Name).HasMaxLength(20);
            modelBuilder.Entity<Role>().Property(p => p.Description).HasMaxLength(100);

            modelBuilder.Entity<Customer>().Property(p => p.FirstName).HasMaxLength(20);
            modelBuilder.Entity<Customer>().Property(p => p.LastName).HasMaxLength(30);
            modelBuilder.Entity<Customer>().Property(p => p.Email).HasMaxLength(100);

            modelBuilder.Entity<Preference>().Property(p => p.Name).HasMaxLength(50);

            modelBuilder.Entity<PromoCode>().Property(p => p.Code).HasMaxLength(50);
            modelBuilder.Entity<PromoCode>().Property(p => p.ServiceInfo).HasMaxLength(100);
        }
    }
}
